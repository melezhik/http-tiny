#!/usr/bin/env raku

use Test;
use HTTP::Tiny;

unless %*ENV<ONLINE_TESTING> {
    say '1..0 # SKIP: ONLINE_TESTING not set';
    exit;
}

my &from-json = { Rakudo::Internals::JSON.from-json: $^a }
my &to-json = { Rakudo::Internals::JSON.to-json: $^a }

my $ua = HTTP::Tiny.new;

my @schemes = 'http';
@schemes.push: 'https' if HTTP::Tiny.can-ssl;

for @schemes -> $scheme {
    for < get post > -> $method {
        do-test 'Default multipart form with file upload',
            $ua."$method"(
                "$scheme://httpbin.org/anything?foo=bar",
                content => {
                    file => 't/text-file.txt'.IO,
                    list => [ 123, 456 ],
                },
            ),
            {
                args  => { foo  => 'bar' },
                files => { file => "Hello World! 🌍\n" },
                form  => { list => [ '123', '456' ] },
            }

        do-test 'Default URL encoded form',
            $ua."$method"(
                "$scheme://httpbin.org/anything?zipi=zape",
                content => {
                    username => 'pat-span',
                    password => 'password',
                },
            ),
            {
                args  => { zipi => 'zape' },
                files => { },
                form  => { username => 'pat-span', password => 'password' }
            }

        do-test 'No body',
            $ua."$method"( "$scheme://httpbin.org/anything" ),
            {
                args  => { },
                files => { },
                form  => { },
            }
    }
}

done-testing;

sub do-test ( $label, $res, %want ) {
    my $content = from-json $res<content>.decode;
    my %have = %want.keys.map: { $_ => $content{$_} }

    subtest "$content<method> $content<url>: $label" => {
        ok $res<success>, 'Request was succesful';
        is-deeply %have, %want, 'Matches expected output';
    }
}
